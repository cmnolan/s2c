from machine import I2C
from machine import Pin
from network import WLAN
from vcnl4010 import VCNL4010
from lis2hh12 import LIS2HH12
from struct import unpack
from network import Sigfox
import pycom
import socket
import time
import machine
from pysense import Pysense


STATE_ACTIVE = const(0)
STATE_ASLEEP = const(0xFF)

STATE_BANK = const(0)
WAKE_COUNT_BANK_H = const(1)
WAKE_COUNT_BANK_L = const(2)
PROX_REST_BANK_H = const(3)
PROX_REST_BANK_L = const(4)
TILT_REST_BANK_H = const(5)
TILT_REST_BANK_L = const(6)
LAST_HALL_BANK = const(7)
HALL_COUNT_BANK = const(8)
HALL_TRIGGERED_BANK = const(9)
HALL_MSG_BANK = const(10)
PROX_MSG_BANK = const(11)
COUNT_PROX_OFF_BANK = const(12)
SEQ_MSG_BANK = const(13)
TOTAL_HALL_BANK = const(14)
TOTAL_PROX_BANK = const(15)
TOTAL_TILT_BANK = const(16)
TILT_COUNT_BANK = const(17)
TILT_MSG_BANK = const(18)
SEND_MSG_BANK = const(19)
HALL_ALARM_BANK = const(20)
PROX_ALARM_BANK = const(21)
TILT_ALARM_BANK = const(22)

def write_16_bit(bank, value):
    pys.write_memory_bank(bank, (value >> 8) & 0xFF)
    pys.write_memory_bank(bank + 1, value & 0xFF)

def read_16_bit(bank):
    h = pys.read_memory_bank(bank)
    l = pys.read_memory_bank(bank + 1)
    return (h << 8) + l

# disable WiFi
WLAN().deinit()

print ('STARTING NOW v2')

i2c = I2C(0, baudrate=50000, pins=('P22', 'P21'))      # Initialize the I2C bus

pys = Pysense(i2c=i2c)

# init Sigfox for RCZ1 (Europe)
sigfox = Sigfox(mode=Sigfox.SIGFOX, rcz=Sigfox.RCZ1)
# create a Sigfox socket
s = socket.socket(socket.AF_SIGFOX, socket.SOCK_RAW)
# make the socket blocking
s.setblocking(True)
# configure it as uplink only
s.setsockopt(socket.SOL_SIGFOX, socket.SO_RX, False)

# check the state
if pys.read_memory_bank(STATE_BANK) != STATE_ASLEEP:
    #Send Start message on reset - should only send on physical reset
    print("Sending Sigfox start message")
    s.send(bytes([170, 170]))

    last_hall = 0
    hall_count = 0
    hall_triggered = 0
    hall_message = 0
    prox_message = 0
    count_Prox_off = 0
    seq_message = 0
    total_hall = 0
    total_prox = 0
    total_tilt = 0
    tilt_count = 0
    tilt_message = 0
    send_message = 0
    hall_alarm = 0
    prox_alarm = 0
    tilt_alarm = 0
    wake_count = 0

    prox_Rest = 0
    tilt_Rest = 0
    wake_count = 0

else:
    print("Waking up from sleep")
    pys.write_memory_bank(STATE_BANK, STATE_ACTIVE)

    last_hall = pys.read_memory_bank(LAST_HALL_BANK)
    hall_count = pys.read_memory_bank(HALL_COUNT_BANK)
    hall_triggered = pys.read_memory_bank(HALL_TRIGGERED_BANK)
    hall_message = pys.read_memory_bank(HALL_MSG_BANK)
    prox_message = pys.read_memory_bank(PROX_MSG_BANK)
    count_Prox_off = pys.read_memory_bank(COUNT_PROX_OFF_BANK)
    seq_message = pys.read_memory_bank(SEQ_MSG_BANK)
    total_hall = pys.read_memory_bank(TOTAL_HALL_BANK)
    total_prox = pys.read_memory_bank(TOTAL_PROX_BANK)
    total_tilt = pys.read_memory_bank(TOTAL_TILT_BANK)
    tilt_count = pys.read_memory_bank(TILT_COUNT_BANK)
    tilt_message = pys.read_memory_bank(TILT_MSG_BANK)
    send_message = pys.read_memory_bank(SEND_MSG_BANK)
    hall_alarm = pys.read_memory_bank(HALL_ALARM_BANK)
    prox_alarm = pys.read_memory_bank(PROX_ALARM_BANK)
    tilt_alarm = pys.read_memory_bank(TILT_ALARM_BANK)

    prox_Rest = read_16_bit(PROX_REST_BANK_H)
    tilt_Rest = read_16_bit(TILT_REST_BANK_H)
    wake_count = read_16_bit(WAKE_COUNT_BANK_H)

#Power test
#time.sleep(60)

i2c.writeto_mem(0x13, 0x83, bytes([0x10]))

vcnl = VCNL4010(i2c=i2c)
acc = LIS2HH12(i2c=i2c)

if not pys.get_button_value():
    print ('In interrupt setting hall_triggered')
    hall_count += 1
else:
    hall_count = 0

p_HS_power = Pin('P10', mode=Pin.OUT)
p_HS_power.value(1)

wake_count += 1

prox_Sum = 0
tilt_Sum = 0
hall_Sum = 0
for num in range(0, 5): # Just to get a decent average
    prox_Sum = prox_Sum + vcnl.read_proximity()
    acc.read()
    tilt_Sum = tilt_Sum + abs(acc.yaw())
#   hall_Sum = hall_Sum + p_in()
prox_Avg = prox_Sum // 5
tilt_Avg = tilt_Sum // 5
print
print ('--------------')
# print ('time', currnet_time)
print ('Battery', pys.read_battery_voltage())
print ('Avg Prox = ' , prox_Avg)
print ('Avg Tilt = ' , tilt_Avg)
if prox_Rest == 0:
    prox_Rest = prox_Avg
if tilt_Rest == 0:
    tilt_Rest = tilt_Avg
print ('prox_Rest', prox_Rest)
print ('tilt_Rest', tilt_Rest)

if abs(tilt_Rest - tilt_Avg) > 5:
    tilt_count = tilt_count + 1
if (tilt_count > 1 and abs(tilt_Rest - tilt_Avg) < 5):
    tilt_count = -2
if (tilt_count < 0 and abs(tilt_Rest - tilt_Avg) < 5):
    tilt_count = tilt_count + 1
elif abs(tilt_Rest - tilt_Avg) < 1:
    tilt_Rest = tilt_Avg
    tilt_count = 0

#If prox_avg > 3400 and Tilt < 10 it's ON simple as
if (prox_Avg > 3400 and tilt_Avg < 5):
    print ('Prox ON')
    prox_Rest = prox_Avg

    if count_Prox_off == -1:
        count_Prox_off = 0
    if count_Prox_off > 0:
        count_Prox_off = -1

elif abs(prox_Rest - prox_Avg) > 200:
    print ('Prox OFF')
    count_Prox_off = count_Prox_off + 1
elif prox_Avg < 2400:
    print ('Prox OFF')
    count_Prox_off = count_Prox_off + 1
else:
    print ('Prox ON')
    count_Prox_off = 0
if abs(prox_Rest - prox_Avg) < 30:
    prox_Rest = prox_Avg

# Do hourly and daily counts of 3 trigger types
# Times Prox changed turn into yes/no per hour
# Hall on/offs or times reached 5
# Times tilt chnaged by more than 10 degrees for more than 10 sec and returned
# Decide on interrupt

# TODO: Use a loop count here instead

if wake_count % 12 == 0:  # hourly heart beat - wake_count every 5 second - do the sums
    pycom.heartbeat(False)
    print ('sending heartbeat')
    seq_message = (seq_message + 1) & 0xFF
    current_angle = int(tilt_Avg)
    prox_MSB = int(prox_Avg / 256)
    prox_LSB = int(prox_Avg % 256)
    s.send(bytes([seq_message, hall_message , total_hall,  prox_message , total_prox , tilt_message , total_tilt, current_angle, prox_MSB, prox_LSB, 11, 12]))
    total_hall = (total_hall + hall_message) & 0xFF
    hall_message = 0
    total_prox = (total_prox + prox_message) & 0xFF
    prox_message = 0
    total_tilt = (total_tilt + tilt_message) & 0xFF
    tilt_message = 0

if hall_count > 3:
    print('Triggered by Hall')
    hall_count = 0
    hall_message = hall_message + 1
    send_message = 1
    hall_alarm = 0x11

if count_Prox_off == 1:
    print('Triggered by Prox OFF')
    prox_message = prox_message + 1
    send_message = 1
    prox_alarm = 0x10

if count_Prox_off == -1:
    print('Triggered by Prox ON')
    prox_message = prox_message + 1
    send_message = 1
    prox_alarm = 0x01

if tilt_count == 2:
    print ('trigger by acc OFF')
    tilt_message = tilt_message + 1
    send_message = 1
    tilt_alarm = 0x10

if tilt_count == -1:
    print ('trigger by acc ON')
    tilt_message = tilt_message + 1
    send_message = 1
    tilt_alarm = 0x01

if send_message == 1:
    current_angle = int(tilt_Avg)
    prox_MSB = int(prox_Avg / 256)
    prox_LSB = int(prox_Avg % 256)
    s.send(bytes([170, hall_alarm , prox_alarm,  tilt_alarm , current_angle , prox_MSB , prox_LSB, 170, 170, 170, 170, 170]))
    send_message = 0
    hall_alarm = 0
    prox_alarm = 0
    tilt_alarm = 0

# Store everything that needs to be persistent and go to sleep
pys.write_memory_bank(LAST_HALL_BANK, last_hall)
pys.write_memory_bank(HALL_COUNT_BANK, hall_count)
pys.write_memory_bank(HALL_TRIGGERED_BANK, hall_triggered)
pys.write_memory_bank(HALL_MSG_BANK, hall_message)
pys.write_memory_bank(PROX_MSG_BANK, prox_message)
pys.write_memory_bank(COUNT_PROX_OFF_BANK, count_Prox_off)
pys.write_memory_bank(SEQ_MSG_BANK, seq_message)
pys.write_memory_bank(TOTAL_HALL_BANK, total_hall)
pys.write_memory_bank(TOTAL_PROX_BANK, total_prox)
pys.write_memory_bank(TOTAL_TILT_BANK, total_tilt)
pys.write_memory_bank(TILT_COUNT_BANK, tilt_count)
pys.write_memory_bank(TILT_MSG_BANK, tilt_message)
pys.write_memory_bank(SEND_MSG_BANK, send_message)
pys.write_memory_bank(HALL_ALARM_BANK, hall_alarm)
pys.write_memory_bank(PROX_ALARM_BANK, prox_alarm)
pys.write_memory_bank(TILT_ALARM_BANK, tilt_alarm)

write_16_bit(PROX_REST_BANK_H, int(prox_Rest))
write_16_bit(TILT_REST_BANK_H, int(tilt_Rest))
write_16_bit(WAKE_COUNT_BANK_H, wake_count)

pys.write_memory_bank(STATE_BANK, STATE_ASLEEP)

pys.setup_sleep(5)
pys.go_to_sleep()
