# Example:
#
# from vcnl4010 import VCNL4010
#
# vcnl = VCNL4010('P10', 'P11')
#     or
#
# from machine import I2C
# i2c = I2C(baudrate=100000, pins=('P10', 'P11'))
# vcnl = VCNL4010(i2c=i2c)
#
# while True:
#     print(vcnl.read_proximity())
#     time.sleep(0.25)
#

from machine import I2C
import binascii
import time
import math


I2CADDR = const(0x13)

# commands and constants
COMMAND = const(0x80)
PRODUCTID = const(0x81)
PROXRATE = const(0x82)
IRLED = const(0x83)
AMBIENTPARAMETER = const(0x84)
AMBIENTDATA = const(0x85)
PROXIMITYDATA = const(0x87)
INTCONTROL = const(0x89)
PROXINITYADJUST = const(0x8A)
INTSTAT = const(0x8E)
MODTIMING = const(0x8F)

# frequency values
FREQ_3M125   = const(3)
FREQ_1M5625  = const(2)
FREQ_781K25  = const(1)
FREQ_390K625 = const(0)

MEASURE_AMBIENT = const(0x10)
MEASURE_PROXIMITY = const(0x08)
AMBIENT_READY = const(0x40)
PROXIMITY_READY = const(0x20)


class VCNL4010:

    def __init__(self, i2c=None, sda=None, scl=None):
        if i2c is not None:
            self.i2c = i2c
        else:
            self.i2c = I2C(0, mode=I2C.MASTER, pins=(sda, scl))
        self.reg = bytearray(1)

        rev = self.i2c.readfrom_mem(I2CADDR, PRODUCTID, 1)
        if (rev[0] & 0xF0 != 0x20):
            raise ValueError("incorrect product ID")

        self.set_led_current(10)    # 100 mA current seems to give the best results
        self.set_frequency(FREQ_390K625)
        self.i2c.writeto_mem(I2CADDR, INTCONTROL, bytes([0x08]))
        time.sleep(0.005)

    def set_frequency(self, freq):
        self.i2c.readfrom_mem_into(I2CADDR, MODTIMING, self.reg)
        self.reg[0] &= ~(0b00011000)
        self.reg[0] |= (freq << 3)
        self.i2c.writeto_mem(I2CADDR, MODTIMING, self.reg)

    def set_led_current(self, current):
        if current > 20:
            current = 20
        self.i2c.writeto_mem(I2CADDR, IRLED, bytes([current]))

    def read_proximity(self):
        self.i2c.readfrom_mem_into(I2CADDR, INTSTAT, self.reg)
        self.reg[0] &= ~0x80
        self.i2c.writeto_mem(I2CADDR, INTSTAT, self.reg)

        self.i2c.readfrom_mem_into(I2CADDR, COMMAND, self.reg)
        self.reg[0] |= MEASURE_PROXIMITY
        self.i2c.writeto_mem(I2CADDR, COMMAND, self.reg)
        while True:
            self.i2c.readfrom_mem_into(I2CADDR, COMMAND, self.reg)
            if self.reg[0] & PROXIMITY_READY:
                proximity = self.i2c.readfrom_mem(I2CADDR, PROXIMITYDATA, 2)
                prx_ctns = int(proximity[0] << 8) + int(proximity[1])
                return prx_ctns # (prx_ctns, 545.5112 / math.pow(prx_ctns, 0.56632))
            time.sleep_ms(1)

    def read_ambient_light(self):
        self.i2c.readfrom_mem_into(I2CADDR, INTSTAT, self.reg)
        self.reg[0] &= ~0x40
        self.i2c.writeto_mem(I2CADDR, INTSTAT, self.reg)

        self.i2c.readfrom_mem_into(I2CADDR, COMMAND, self.reg)
        self.reg[0] |= MEASURE_AMBIENT
        self.i2c.writeto_mem(I2CADDR, COMMAND, self.reg)

        while True:
            self.i2c.readfrom_mem_into(I2CADDR, COMMAND, self.reg)
            if self.reg[0] & AMBIENT_READY:
                light = self.i2c.readfrom_mem(I2CADDR, AMBIENTDATA, 2)
                return (int(light[0] << 8) + int(light[1])) / 4
            time.sleep_ms(1)
